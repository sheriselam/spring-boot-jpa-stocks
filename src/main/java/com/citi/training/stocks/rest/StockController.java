package com.citi.training.stocks.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stocks.model.Stock;
import com.citi.training.stocks.repo.StockDao;

@RestController
@RequestMapping("/stocks")
public class StockController {

	private static final Logger LOG = 
			LoggerFactory.getLogger(StockController.class);
	
	@Autowired
	StockDao sb;
	
    @RequestMapping(method=RequestMethod.GET)
	public Iterable<Stock> findAll(){
    	LOG.info("HTTP GET to findAll()");
		return sb.findAll();	
	}
	
    @RequestMapping(value="/{id}",method=RequestMethod.GET)
	public Stock findById(@PathVariable long id) {
    	LOG.info("HTTP GET to findById()");
    	return sb.findById(id).get();
	}
    
    @RequestMapping(method=RequestMethod.POST)
	public void create(@RequestBody Stock stock) {
    	LOG.info("HTTP POST to create()");
    	sb.save(stock);
	}
	
	@ResponseBody
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
	public void deleteById(@PathVariable long id) {
    	LOG.info("HTTP DELETE to deleteById()");
    	sb.deleteById(id);
	}
}

