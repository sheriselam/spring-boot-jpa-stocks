package com.citi.training.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStocksApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStocksApplication.class, args);
	}

}
